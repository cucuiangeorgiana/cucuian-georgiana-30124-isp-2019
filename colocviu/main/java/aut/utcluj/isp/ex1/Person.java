public class Person {
    private String firstName;
    private String lastName;

    public Person(String firstName) {
        
         try
        {this.lastName = "";
        this.firstName =firstName;}
        catch (ExceptionInInitializerError e)
        {throw new UnsupportedOperationException("Not supported yet.");}
    }

    public Person(String firstName, String lastName) {
        
         try {this.firstName=firstName;
        this.lastName=lastName;}
       catch(ExceptionInInitializerError e)
       {throw new UnsupportedOperationException("Not supported yet.");}
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
    
    public boolean equals(Object o) {

        // If the object is compared with itself then return true
        if (o == this) {
            return true;
        }

        /* Check if o is an instance of Complex or not
          "null instanceof [type]" also returns false */
        if (!(o instanceof Person)) {
            return false;
        }

        // typecast o to Complex so that we can compare data members
        Person c = (Person) o;

        // Compare the data members and return accordingly
        return firstName.equals(c.firstName)
                && lastName.equals(c.lastName);
    }
    
     public String toString()
    {
        return this.firstName+" "+this.lastName;
        
    }
}