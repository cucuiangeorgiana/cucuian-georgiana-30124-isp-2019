package aut.utcluj.isp.ex3;

/**
 * @author stefan
 */
public class Employee {
    private String firstName;
    private String lastName;
    private Double salary;
    private String cnp;

    public Employee(String firstName, String lastName, Double salary, String cnp) {
        this.firstName=firstName;
        this.lastName=lastName;
        this.salary=salary;
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String getFirstName() {
        return this.firstName;
    }

     public void setSalary(Double salary){
     this.salary=salary;
     }
     
    public String getLastName() {
        return this.lastName;
    }

    public Double getSalary() {
        return this.salary;
    }

    public String getCnp() {
        return cnp;
    }
}
