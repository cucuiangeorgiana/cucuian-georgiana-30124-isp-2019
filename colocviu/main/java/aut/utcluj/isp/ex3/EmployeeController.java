package aut.utcluj.isp.ex3;

import java.util.*;

/**
 * @author stefan
 */
public class EmployeeController {
  List<Employee> people = new ArrayList<Employee>();
  
    /**
     * Add new employee to the list of employees
     *
     * @param employee - employee information
     */
    public void addEmployee(final Employee employee) {
        people.add(employee);
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Get employee by cnp
     *
     * @param cnp - unique cnp
     * @return found employee or null if not found
     */
    public Employee getEmployeeByCnp(final String cnp) {
        for(Employee e:people){
        if (e.getCnp().equals(cnp)) return e;
        else return null;
        }
        
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Update employee salary by cnp
     *
     * @param cnp    - unique cnp
     * @param salary - salary
     * @return updated employee
     */
    public Employee updateEmployeeSalaryByCnp(final String cnp, final Double salary) {
        for(Employee e:people){
        if (e.getCnp().equals(cnp)) {e.setSalary(salary); ;return e;}
        else return null;
        }
        
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Delete employee by cnp
     *
     * @param cnp - unique cnp
     * @return deleted employee or null if not found
     */
    public Employee deleteEmployeeByCnp(final String cnp) {
        for(Employee e:people){
        if (e.getCnp().equals(cnp)) people.remove(e);
        else return null;
        }
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Return current list of employees
     *
     * @return current list of employees
     */
    public List<Employee> getEmployees() {
        try{return people;}
        catch(Exception e){
        throw new UnsupportedOperationException("Not supported yet.");}
    }

    /**
     * Get number of employees
     *
     * @return - number of registered employees
     */
    public int getNumberOfEmployees() {
        return people.size();
        
        //throw new UnsupportedOperationException("Not supported yet.");}
    }}

