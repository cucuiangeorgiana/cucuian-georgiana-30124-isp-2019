package aut.utcluj.isp.ex4;

/**
 * @author stefan
 */
public class Employee {
    private String firstName;
    private String lastName;
    private String cnp;
    private SalaryInfo sal = new SalaryInfo();

    public Employee(String firstName, String lastName, String cnp, Double monthlyRevenue) {
          this.lastName = lastName;
         this.firstName = firstName;
        // sal.setMonth( monthlyRevenue);
         this.cnp=cnp;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCnp() {
        return cnp;
    }

    /**
     * Add salary to the employee
     */
    public void addSalary() {
       sal.addSalary();
    }

    /**
     * Add money as bonus to the employee
     * Value added should be positive
     *
     * @param money - money to be added
     */
    public void addMoney(final Double money)  {
        sal.addMoney(money);
    }

    /**
     * Pay tax from salary
     *
     * @param tax - tax to be paid
     */
    public void payTax(final Double tax) {
        sal.payTax(tax);
    }

    /**
     * Get salary info
     *
     * @return salary info
     */
    public SalaryInfo getSalaryInfo() {
        return sal;
    }
}