/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package georgiana.cucuian.lab5.ex1;

/**
 *
 * @author Sala 310
 */
public abstract class Shape {
    protected String color;
    protected boolean filled;
    
    public Shape()
    {
    this.color="red";
    this.filled=true;
    }
    
    public Shape(String color, boolean filled){
       this.color=color;
       this.filled=filled;
    }
    
    public String getColor()
    {
    return this.color;
    }
    
    public void setColor(String color)
    {
        this.color=color;
    }
    
    
    
    
    public boolean isFilled()
    {
        return this.filled;
    }
    
    public void setFilled(boolean filled)
    {
    this.filled=filled;
    }
    
    abstract double getArea();
    abstract double getPerimeter();
    @Override
    public String toString()
    {
    return "Shape with a color:" + this.color+ " and filled: " +this.filled;
    }
}
