/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package georgiana.cucuian.lab5.ex1;

/**
 *
 * @author Sala 310
 */
public class Square extends Rectangle {
   
    public Square() {
		this.setLength(1.0);
		this.setWidth(1.0);
	}
	public Square(double side) {
		this.setLength(side);
		this.setWidth(side);
	}
	public Square(double side,String color,boolean filled){
		this.setLength(side);
		this.setWidth(side);
		this.setColor(color);
		this.setFilled(filled);
	}
	public double getSide() {
		return super.getLength();
	}
	public void setSide(double side) {
		super.setLength(side);
		super.setWidth(side);
	}
	public double getLength() {
		return this.getSide();
	}
	public void setLength(double side) {
		this.setSide(side);
	}
           @Override
	public double getWidth() {
		return this.getSide();
	}
        @Override
	public void setWidth(double side) {
		this.setSide(side);
	}
           @Override
	public String toString() {
		return " A Square with side = " + this.getSide() + ", which is a subclass of" + super.toString();
	}
}
