/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package georgiana.cucuian.lab5.ex2;

/**
 *
 * @author Sala 310
 */
public class ProxyImage implements Image{
 
   private RealImage realImage;
   private RotateImage rotateImage;
   private String fileName;
 
   public ProxyImage(String fileName,int a){
      this.fileName = fileName;
      RealImage real=new RealImage(fileName);
      RotateImage rotated=new RotateImage(fileName);
      
      if (a==1) real.display();//real
      else if (a==2) rotated.display();//rotated
   }
 
   @Override
   public void display() {
      if(realImage == null){
         realImage = new RealImage(fileName);
      }
      realImage.display();
   }
}