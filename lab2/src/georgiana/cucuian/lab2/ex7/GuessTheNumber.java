/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package georgiana.cucuian.lab2.ex7;
import java.util.Random;
import java.util.Scanner;
/**
 *
 * @author cucui
 */
public class GuessTheNumber {
    
public static int randInt(int min, int max) {

    Random rand= new Random();
    int randomNum = rand.nextInt((max - min) + 1) + min;
    return randomNum;
}

 public static void main(String[] args){
     Scanner in=new Scanner(System.in);
 System.out.println("Alege intervalul in care vrei sa fie numarul: ");
 System.out.println("minumul:"); int min=in.nextInt();
 System.out.println("maximul:"); int max=in.nextInt();
 int numarul=randInt(min,max); //"numarul" este numarul ce trebuie ghicit

 int ok=0,i=3;// ok=0: numarul nu a fost inca ghicit
 int guess;
 while ( (ok==0) && (i!=0)){
  System.out.println("scrie un numarul:");
  guess=in.nextInt(); 
  if (guess==numarul) {System.out.println("YOU WON!");ok=1;break;}
  else if (guess<numarul)  System.out.println("Wrong answer, your number it too low");
  else if (guess>numarul)  System.out.println("Wrong answer, your number it too high");
  i--;
 }
 if (i==0)  System.out.println("YOU LOST!, the number was:"+numarul);
 }
}
