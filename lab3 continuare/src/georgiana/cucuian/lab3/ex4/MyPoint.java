/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package georgiana.cucuian.lab3.ex4;
import java.lang.Math; 
/**
 *
 * @author Sala 310
 */
public class MyPoint {
    private int x;
    private int y;
    
    public MyPoint(){
    this.x=0;
    this.y=0;
    }
    
    public MyPoint(int x,int y){
    this.x=x;
    this.y=y;
    }
    
    public int getX(){
    return this.x;
    }
    
    public int getY(){
    return this.y;
    }
    
    public void setX(int x){
    this.x=x;
    }
      public void setY(int y){
    this.y=y;
    }
     public void setXY(int x,int y){
     this.x=x;
     this.y=y;
     }
    
     @Override
     public String toString(){
     return " ( "+getX()+" , "+getY() +" ) ";
     }
     
     public double distance(int x,int y){
     return Math.sqrt((getX()-x)*(getX()-x)+(getY()-y)*(getY()-y));
     }
     
     public double distance(MyPoint point){
     return Math.sqrt((getX()-point.x)*(getX()-point.x)+(getY()-point.y)*(getY()-point.y));
     }
}
