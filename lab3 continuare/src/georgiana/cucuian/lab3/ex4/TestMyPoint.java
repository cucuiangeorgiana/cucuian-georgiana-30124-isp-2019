/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package georgiana.cucuian.lab3.ex4;

/**
 *
 * @author Sala 310
 */
public class TestMyPoint {
    
    public static void main(String[] args){
    MyPoint p1=new MyPoint();
    System.out.println("no-arg: "+p1);
    MyPoint p2=new MyPoint(1,2);
    System.out.println("given 'x' and 'y': "+p2);
    
    System.out.println("first method 'distance' from Point2(1,2) to (3,5) "+ p2.distance(3, 5));
    MyPoint p3=new MyPoint(3,5);
    System.out.println("second method 'distance' from Point2(1,2) to (3,5) "+ p2.distance(p3));
    }
}

