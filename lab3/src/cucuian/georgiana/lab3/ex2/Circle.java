/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cucuian.georgiana.lab3.ex2;

/**
 *
 * @author Sala 310
 */
public class Circle {
    private double radius;
    private String color;
    
    public Circle(){   //default
    this.radius=1.0;
    this.color="red";
    }
    
    public Circle(double radius,String color){ 
    this.radius=radius;
    this.color=color;
    }
    
    public Circle(double radius){
    this.radius=radius;
    }
    
    public Circle(String color){
    this.color=color;
    }
    public double getRadius(){
    return this.radius;}
    
     public double getArea(){
    return (3.14*radius*radius);}

}
