/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package georgiana.cucuian.lab4.ex2;

/**
 *
 * @author cucui
 */
public class Author {
    private String name,email;
	private char genger;
	public Author(String name, String email,char genger)
	{
		this.name=name;
		this.email=email;
		this.genger=genger;
	}
	public String getName() {
	       return name;
	}
	public String getEmail() {
	       return email;
	}
	public char getGenger() {
	       return genger;
	}
	public void setEmail(String email) {
        this.email = email;
    }
	public String toString()
	{
		return "Author-"+name+"("+genger+") at "+ email;
	}
	
}
