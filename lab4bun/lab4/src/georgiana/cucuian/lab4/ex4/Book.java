/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package georgiana.cucuian.lab4.ex4;

/**
 *
 * @author cucui
 */
public class Book {
    private String name;
	//private Author author;
	private Author[] authors;
	private double price;
	private int qtyInStock=0;
	static int c=0;
	public Book(String name, Author[] authors, double price) {
		c++;
		this.name = name;
		this.authors = authors;
		this.price = price;
	}
	public Book(String name, Author[] authors, double price, int qtyInStock) {
		this.name = name;
		this.authors = authors;
		this.price = price;
		this.qtyInStock = qtyInStock;
	}
	public String getName() {
		return name;
	}
	public Author[] getAuthor() {
		return authors;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getQtyInStock() {
		return qtyInStock;
	}
	public void setQtyInStock(int qtyInStock) {
		this.qtyInStock = qtyInStock;
	}
	@Override
	/*
	 * public String toString() { return name + " " + getAuthor() + " " + price; }
	 */
	public String toString() {
		return "Book-"+ name + " by " + c + " authors ";
	}
	void printAuthor() {
		for(int i=1;i<authors.length;i++)
		{
			System.out.println(authors[i].getName());
		}
	}
}
