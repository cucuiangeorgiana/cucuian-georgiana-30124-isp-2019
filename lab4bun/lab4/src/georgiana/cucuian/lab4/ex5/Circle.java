/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package georgiana.cucuian.lab4.ex5;

/**
 *
 * @author cucui
 */
public class Circle {
    private double radius=1.0;
	private String color="red";
	public Circle()
	{
		System.out.println("S-a creat cercul ");
	}
	public String getColor() {
		return color;
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}
	public Circle(double radius)
	{
		super();
		this.radius=radius;
	}
	public double getRadius() {
        return radius;
    }
	public double getArea() {
        return 2*Math.PI*radius;
    }
}