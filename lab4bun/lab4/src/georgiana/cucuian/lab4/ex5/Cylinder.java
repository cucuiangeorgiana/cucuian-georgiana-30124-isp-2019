/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package georgiana.cucuian.lab4.ex5;

/**
 *
 * @author cucui
 */
public class Cylinder extends Circle{
    private double height=1.0;
	public Cylinder() {
		System.out.println("S-a creat un cilindru");
	}
	public Cylinder(double radius) {
		this.setRadius(radius);
	}
	public Cylinder(double radius,double height)
	{
		this.setRadius(radius);
		this.height=height;
	}
	public double getHeight() {
		return height;
	}
	
	public double getVolume() {
		double Volume=Math.PI*Math.pow(getRadius(), 2)*getHeight();
		return Volume;
	}
	public double getArea() {
		double Area=Math.PI*Math.pow(getRadius(), 2);
		return Area;
	}
	public String toString()
	{
		return "Radius="+this.getRadius()+", color="+this.getColor()+", height="+this.getHeight();
		
	}
}
