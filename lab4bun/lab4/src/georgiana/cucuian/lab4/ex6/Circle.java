/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package georgiana.cucuian.lab4.ex6;

/**
 *
 * @author cucui
 */
public class Circle extends Shape{
    double radius=1.0;
	public Circle() {
		this.radius=1.0;
	}
	public Circle(double radius) {
		this.radius=radius;
	}
	public Circle(double radius,String color,boolean filled) {
		this.radius=radius;
		this.setColor(color);
		this.setFilled(filled);
		
	}
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}
	public double getArea() {
		double Area=Math.PI*Math.pow(radius, 2);
		return Area;
	}
	public double getPerimeter() {
		double Perimeter=2*Math.PI*radius;
		return Perimeter;
	}
	public String toString() {
		return " A Circle with radius = " + this.radius + ", which is a subclass of "+ super.toString();
	}
}

