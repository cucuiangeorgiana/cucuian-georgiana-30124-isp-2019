/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package georgiana.cucuian.lab4.ex6;

/**
 *
 * @author cucui
 */
public class Rectangle extends Shape {
    private double width=1.0;
	private double length=1.0;
	public Rectangle() {
		width=length=1;
	}
	public Rectangle(double width,double length) {
		this.width=width;
		this.length=length;
	}
	public Rectangle(double width,double length,String color,boolean filled) {
		
		this.width=width;
		this.length=length;
		this.setColor(color);
		this.setFilled(filled);
	}
	public double getWidth() {
		return width;
	}
	public void setWidth(double width) {
		this.width=width;
	}
	public double getLength() {
		return length;
	}
	public void setLength(double length) {
		this.length=length;
	}
	public double getArea() {
		return length*width;
	}
	public double getPerimeter() {
		return 2*width+2*length;
	}
	public String toString() {
		return " A Rectangle with width = " + width + " and length = " + length + ", which is a subclass of " + super.toString();
	}
}
