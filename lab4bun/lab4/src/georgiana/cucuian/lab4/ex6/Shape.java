/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package georgiana.cucuian.lab4.ex6;

/**
 *
 * @author cucui
 */
public class Shape {
    private String color="red";
	private boolean filled=true;
	public Shape() {
		this.color="green";
		this.filled=true;
	}
	public Shape(String color,boolean filled) {
		this.color=color;
		this.filled=filled;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color=color;
	}
	public boolean isFilled() {
		return filled;
	}
	public void setFilled(boolean filled) {
		this.filled=filled;
	}
	public String toString() {
		if(filled==true)
			return " A Shape with color of "+color+" and filled ";
		else
			return " A Shape with  color of " + color + " and not filled ";

	}
}
