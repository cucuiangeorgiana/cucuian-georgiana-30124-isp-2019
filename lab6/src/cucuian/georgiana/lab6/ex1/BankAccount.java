/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cucuian.georgiana.lab6.ex1;

/**
 *
 * @author Sala 310
 */
public class BankAccount {

    private String owner;
    private double balance;

    BankAccount (String owner,double balance){
		this.owner = owner;
		this.balance = balance;
	}

    @Override
    public String toString() {
        return "BankAccount{" + "owner=" + owner + ", balance=" + balance + '}';
    }
    public void withdraw(double amount) {
        balance = balance - amount;
    }

    public void deposit(double amount) {
        balance = balance + amount;
    }

    public boolean equals(Object obj) {
        if (obj instanceof BankAccount) {
            BankAccount p = (BankAccount) obj;
            return owner == p.owner;
        }
        return false;
    }

    public int hashCode(){
        return (int)Math.round(balance )+ owner.hashCode();
    }

}